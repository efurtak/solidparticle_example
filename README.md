# README #

Created from Hakan Nilsson's programming tutorial:  https://pingpong.chalmers.se/public/pp/public_courses/course08331/published/1504283117033/resourceId/3987163/content/UploadedResources/implementApplication-1.pdf

#INSTRUCTIONS#

To run, build in the solver directory:

wmake

In the casefile directory:

blockMesh

setFields

solidParticleInterFoam

foamToVTK

paraview